using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using DG.Tweening;

public class TOTKMenu : MonoBehaviour
{
    public Camera camera;
    private float duration = 5f;
    private float tweenDuration = 0f;

    private VisualElement root;
    private VisualElement titleScreen;
    private Label Z;
    private Button startGame;
    private Button options;
    private Button amiibo;

    private VisualElement settings;
    private Button invertVertical;
    private Button invertHorizontal;
    private Button cameraSensitivity;
    private Button motionControls;

    private Button abilityVertical;
    private Button abilityHorizontal;
    private Button jumpControls;

    private Button lockMinimap;
    private Button HUD;
    private Button messageWindow;
    private Button voiceLanguage;

    private Button back;

    private int InvertVerCount = 0;
    private int InvertHoriCount = 0;
    private int MotionControlCount = 0;
    private int VerticalAbilityCount = 0;
    private int HorizontalAbilityCount = 0;
    private int JumpControlCount = 0;
    private int LockCount = 0;
    private int HUDCount = 0;
    private int messageCount = 0;
    private int changeSensitivity = 0;
    private int voiceChange = 0;

    // Start is called before the first frame update
    void Start()
    {
        DOTween.SetTweensCapacity(1250,50);
        root = GetComponent<UIDocument>().rootVisualElement;
        titleScreen = root.Q<VisualElement>("titleScreen");
        startGame = root.Q<Button>("NewGame");
        options = root.Q<Button>("Options");
        amiibo = root.Q<Button>("Amiibo");
        titleScreen.RemoveFromClassList("close-menu");

        // startGame.RegisterCallback<ClickEvent>(startClicked);
        options.RegisterCallback<ClickEvent>(optionClicked);
        // amiibo.RegisterCallback<ClickEvent>(amiiboClicked);
        
        settings = root.Q<VisualElement>("settingsMenu");
        // Add all buttons
        invertVertical = settings.Q<Button>("InvertVerticalCamera");
        invertHorizontal = settings.Q<Button>("InvertHorizontalCamera");
        cameraSensitivity = settings.Q<Button>("CameraSensitivity");
        motionControls = settings.Q<Button>("AimWithMotionControls");
        abilityVertical = settings.Q<Button>("InvertedVerticalAbilityControls");
        abilityHorizontal = settings.Q<Button>("InvertedHorizontalAbilityControls");
        jumpControls = settings.Q<Button>("SwapJumpControls");
        lockMinimap = settings.Q<Button>("LockMinimapToNorth");
        HUD = settings.Q<Button>("HUDMode");
        messageWindow = settings.Q<Button>("MessageWindowTransparency");
        voiceLanguage = settings.Q<Button>("VoiceLanguage");
        back = settings.Q<Button>("Back");
        
        invertVertical.RegisterCallback<ClickEvent>(invertVert);
        invertHorizontal.RegisterCallback<ClickEvent>(invertHoriz);
        cameraSensitivity.RegisterCallback<ClickEvent>(cameraSenseClicked);
        motionControls.RegisterCallback<ClickEvent>(MotionControl);
        abilityVertical.RegisterCallback<ClickEvent>(abilityVert);
        abilityHorizontal.RegisterCallback<ClickEvent>(abilityHoriz);
        jumpControls.RegisterCallback<ClickEvent>(Jump);
        lockMinimap.RegisterCallback<ClickEvent>(Minimap);
        HUD.RegisterCallback<ClickEvent>(HUDClicked);
        messageWindow.RegisterCallback<ClickEvent>(messageClicked);
        voiceLanguage.RegisterCallback<ClickEvent>(voiceClicked);
        back.RegisterCallback<ClickEvent>(backClicked);
    
    }

    private void Update()
    {
        camera.DOColor(Color.grey, duration);
        tweenDuration += 1 * Time.deltaTime;
        if (tweenDuration > 5)
        {
            camera.DOColor(Color.clear,duration);
        }
        
    }
    private void optionClicked(ClickEvent option)
    {
        // Make settings appear
        titleScreen.AddToClassList("close-menu");
        settings.AddToClassList("open-settings");
        Debug.Log("Option Clicked");
        
    }
    private void invertVert(ClickEvent enable)
    {
        invertVertical.text = "Enabled";
        InvertVerCount += 1;
        if (InvertVerCount % 2 == 0)
        {
            invertVertical.text = "Disabled";
        }
    }
    private void invertHoriz(ClickEvent enable)
    {
        invertHorizontal.text = "Enabled";
        InvertHoriCount += 1;
        if (InvertHoriCount % 2 == 0)
        {
            invertHorizontal.text = "Disabled";
        }
    }
    private void cameraSenseClicked(ClickEvent enable)
    {
        cameraSensitivity.text = "High";
        changeSensitivity += 1;
        if (changeSensitivity % 2 == 0)
        {
            cameraSensitivity.text = "Very High";
        }
        if (changeSensitivity % 3 == 0)
        {
            cameraSensitivity.text = "Very Low";
        }
        if (changeSensitivity % 4 == 0)
        {
            cameraSensitivity.text = "Low";
        }
        if (changeSensitivity % 5 == 0)
        {
            cameraSensitivity.text = "Normal";
            changeSensitivity = 0;
        }
        
    }
    private void MotionControl(ClickEvent enable)
    {
        motionControls.text = "Enabled";
        MotionControlCount += 1;
        if (MotionControlCount % 2 == 0)
        {
            motionControls.text = "Disabled";
        }
    }
    private void abilityVert(ClickEvent enable)
    {
        abilityVertical.text = "Enabled";
        VerticalAbilityCount += 1;
        if (VerticalAbilityCount % 2 == 0)
        {
            abilityVertical.text = "Disabled";
        }
    }
    private void abilityHoriz(ClickEvent enable)
    {
        abilityHorizontal.text = "Enabled";
        HorizontalAbilityCount += 1;
        if (HorizontalAbilityCount % 2 == 0)
        {
            abilityHorizontal.text = "Disabled";
        }
    }
    private void Jump(ClickEvent enable)
    {
        jumpControls.text = "Enabled";
        JumpControlCount += 1;
        if (JumpControlCount % 2 == 0)
        {
            jumpControls.text = "Disabled";
        }
    }
    private void Minimap(ClickEvent enable)
    {
        lockMinimap.text = "Enabled";
        LockCount += 1;
        if (LockCount % 2 == 0)
        {
            lockMinimap.text = "Disabled";
        }
    }
    private void HUDClicked(ClickEvent enable)
    {
        HUD.text = "Pro";
        HUDCount += 1;
        if (HUDCount % 2 == 0)
        {
            HUD.text = "Normal";
        }
    }
    private void messageClicked(ClickEvent enable)
    {
        messageWindow.text = "Dark";
        messageCount += 1;
        if (messageCount % 2 == 0)
        {
            messageWindow.text = "Normal";
        }
    }
    private void voiceClicked(ClickEvent enable)
    {
        voiceLanguage.text = "French (France)";
        voiceChange += 1;
        if (voiceChange % 2 == 0)
        {
            voiceLanguage.text = "French (Canada)";
        }
        if (voiceChange % 3 == 0)
        {
            voiceLanguage.text = "German";
        }
        if (voiceChange % 4 == 0)
        {
            voiceLanguage.text = "Spanish (Spain)";
        }
        if (voiceChange % 5 == 0)
        {
            voiceLanguage.text = "Spanish (Latin America)";
        }
        if (voiceChange % 6 == 0)
        {
            voiceLanguage.text = "Italian";
        }
        if (voiceChange % 7 == 0)
        {
            voiceLanguage.text = "Russian";
        }
        if (voiceChange % 8 == 0)
        {
            voiceLanguage.text = "Japanese";
        }
        if (voiceChange % 9 == 0)
        {
            voiceLanguage.text = "English";
            voiceChange = 0;
        }
    }
    private void backClicked(ClickEvent back)
    {
        settings.RemoveFromClassList("open-settings");
        titleScreen.RemoveFromClassList("close-menu");
    }
}

