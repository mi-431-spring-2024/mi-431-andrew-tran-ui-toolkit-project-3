Project - UI Toolkit Research Phase

Your Name - Andrew Tran

Date - March 11, 2024

What was/were your starting tutorial(s)?

Animted UI
https://www.youtube.com/watch?v=qm59GPmNtek 
Game Menu
https://www.youtube.com/watch?v=8w0qvO4Vumc


What did you do in your Research Phase?
Learned the basics of UI Toolkit



What did you do in your Creative Phase?
(leave blank during Research Phase)


Any assets used that you didn't create yourself?  (art, music, etc. Just tell us where you got it, link it here)




Did you receive help from anyone outside this class?  (list their names and what they helped with)




Did you get help from any AI Code Assistants?  (Tell us which .cs file to look in for the citation and describe what you learned; also be sure to comment in the .cs per the syllabus instructions)




Did you get help from any additional online websites, videos, or tutorials?  (link them here)



What trouble did you have with this project?



Is there anything else we should know?

